var express = require('express');
var app = express();
var router = require('./router/main')(app);
var mongoose = require('mongoose');
//db
mongoose.connect("mongodb://localhost/userdb");
var UserController = require('./user/UserController');
 //app
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
app.use(express.static('public'));
app.use('/users', UserController);

var port = process.env.PORT || 3000;
var server = app.listen(port, function() {
    console.log('서버 기동 :' + port);
});


